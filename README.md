# FactorySolutions

# Проект на ветке main

**Реализованы все аспекты из базовых задач кроме одной 
"обработки активных рассылок и отправки сообщений клиентам" - непонятная постановка задачи**


**А также из допов:

1.организовать тестирование написанного кода <-- Done -->


2.сделать так, чтобы по адресу /docs/ открывалась страница со Swagger UI и в нём отображалось описание разработанного API. Пример: https://petstore.swagger.io <-- Done [api/docs, api/redoc] -->

3.реализовать администраторский Web UI для управления рассылками и получения статистики по отправленным сообщениям
<-- Done (p.s. django admin + celery beat tables for control all perdiodic tasks) -->


4.реализовать дополнительный сервис, который раз в сутки отправляет статистику по обработанным рассылкам на email
<-- Done (p.s. django send email + celery beat -->

5.удаленный сервис может быть недоступен, долго отвечать на запросы или выдавать некорректные ответы. Необходимо организовать обработку ошибок и откладывание запросов при неуспехе для последующей повторной отправки. Задержки в работе внешнего сервиса никак не должны оказывать влияние на работу сервиса рассылок.
<-- Done (p.s. на каждое сообщение даю три попытки на отправку если же сервис не допутен то задача попробует себя через 6 часов) -->


6.обеспечить подробное логирование на всех этапах обработки запросов, чтобы при эксплуатации была возможность найти в логах всю информацию по
<-- Done (logging + console) -->

•
id рассылки - все логи по конкретной рассылке (и запросы на api и внешние запросы на отправку конкретных сообщений)

•
id сообщения - по конкретному сообщению (все запросы и ответы от внешнего сервиса, вся обработка конкретного сообщения)

•
id клиента - любые операции, которые связаны с конкретным клиентом (добавление/редактирование/отправка сообщения/…)**



## Getting started --- Инструкция поо запуску проекта

**First you must clone project **

```
> git clone https://gitlab.com/a.abitbek/factorysolutions.git

> cd projectDir

```
# [Second create venv or conda as virtual env](https://docs.python.org/3/library/venv.html)
# [Third install redis and run with redis-server(you must check)](https://redis.io/docs/getting-started/installation/)

**After install all requirements from requirements.txt and run this commands in projectDir**
```
> pip install -r requirements.txt
> python manage.py makemigrations
> python manage.py migrate
> python manage.py createsuperuser
> python manage.py runserver


<-- FROM ANOTHER TERMINAL -->
> celery -A FactorySolutions worker --loglevel=info

<-- FROM ANOTHER TERMINAL -->
> celery -A FactorySolutions beat -l info --scheduler django_celery_beat.schedulers:DatabaseScheduler

```

#Описание реализованных методов в формате OpenAPI --- api/docs or api/redoc

#That's a`ll 
