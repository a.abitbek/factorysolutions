from django.contrib import admin
from .models import Mailing, Client, Message
# Register your models here.

class MailingAdmin(admin.ModelAdmin):
    list_display = ['start_datetime', 'message_text', 'client_filter_operator_code', 'client_filter_tag', 'end_datetime']

class ClientAdmin(admin.ModelAdmin):
    list_display = ['phone_number', 'operator_code', 'tag', 'timezone']

class MsgAdmin(admin.ModelAdmin):
    list_display = ['creation_datetime', 'delivery_status', 'mailing', 'client']



admin.site.register(Mailing, MailingAdmin)
admin.site.register(Client, ClientAdmin)
admin.site.register(Message, MsgAdmin)