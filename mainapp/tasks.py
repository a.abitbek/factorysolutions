import logging
from celery import shared_task
from django.core.mail import send_mail
from django.db.models import Q
import requests
import json
from datetime import datetime, timedelta
from django_celery_beat.models import IntervalSchedule, PeriodicTask
from requests.exceptions import RequestException
import time
from FactorySolutions import settings


def send_message(url, headers, payload, id, max_retries=3, retry_delay=1,):
    logger = logging.getLogger(__name__)
    for attempt in range(max_retries):
        try:
            logger.info(f"Sending request to {url} with payload: {payload}")
            response = requests.post(url, headers=headers, data=payload)
            logger.info(f"Received response with status code: {response.status_code}")
            logger.debug(f"Response content: {response.content.decode('utf-8')}")
            response.raise_for_status()
            return response
        except RequestException as e:
            print(f"Error sending message: {e}")
            if attempt < max_retries - 1:
                print(f"Retrying in {retry_delay} seconds...")
                time.sleep(retry_delay)
            else:
                print("Maximum retries reached. Failed to send message.")
                interval, _ = IntervalSchedule.objects.get_or_create(
                    every=100,
                    period=IntervalSchedule.DAYS,
                )
                PeriodicTask.objects.create(
                    interval=interval,
                    name=f'Павторная Рассылка #{id}',
                    task='mainapp.tasks.sendMailing',
                    args=[id],
                    start_time=datetime.now() + timedelta(hours=6),
                    one_off=True,
                )


@shared_task
def sendMailing(id):
    from .models import Mailing, Client, Message
    mail = Mailing.objects.get(pk=id)
    clients = Client.objects.filter(Q(operator_code=mail.client_filter_operator_code) | Q(tag=mail.client_filter_tag))
    headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE3MjA2MDc5MzYsImlzcyI6ImZhYnJpcXVlIiwibmFtZSI6Imh0dHBzOi8vdC5tZS9hYWJpdGJlayJ9.Kdfmu3WbqHzENr2dNNhXNLL43Kyc7-wpQRQRCuv3_K0'
    }
    for client in clients:
        msg = Message.objects.create(
            mailing=mail,
            client=client
        )
        url = f"https://probe.fbrq.cloud/v1/send/{msg.pk}"
        payload = json.dumps({
            "id": msg.pk,
            "phone": client.phone_number,
            "text": mail.message_text
        })
        response = send_message(url, headers, payload, id)
        print(f"[INFO -- id: {msg.pk}  phone: {client.phone_number}  SEND] RESPONSE {response.status_code}, TEXT {response.text}" )
        msg.delivery_status = Message.Status.sent if response.status_code == 200 else Message.Status.notSent
        msg.save()
    mail.end_datetime = datetime.now().replace(tzinfo=mail.start_datetime.tzinfo)
    mail.save()


@shared_task
def send_stats(recipient_list):
    from mainapp.models import Mailing, Message
    subject = 'Статистика рассылок'
    mailing_count = Mailing.objects.count()
    sent_messages_count = Message.objects.filter(delivery_status=Message.Status.sent).count()
    unsent_messages_count = Message.objects.filter(delivery_status=Message.Status.notSent).count()
    message = f"Число рассылок {mailing_count}, отправлено {sent_messages_count}, неотпрвалено {unsent_messages_count}"
    from_email = settings.EMAIL_HOST_USER
    if recipient_list:
        send_mail(subject, message, from_email, recipient_list)
    else:
        print("А кому отправлять то?")

