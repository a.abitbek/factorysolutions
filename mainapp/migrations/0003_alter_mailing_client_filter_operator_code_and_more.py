# Generated by Django 4.2.3 on 2023-07-11 04:13

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('mainapp', '0002_alter_client_operator_code_alter_client_tag_and_more'),
    ]

    operations = [
        migrations.AlterField(
            model_name='mailing',
            name='client_filter_operator_code',
            field=models.CharField(blank=True, max_length=10, null=True, verbose_name='Код оператора'),
        ),
        migrations.AlterField(
            model_name='mailing',
            name='client_filter_tag',
            field=models.CharField(blank=True, max_length=255, null=True, verbose_name='Тэг'),
        ),
        migrations.AlterField(
            model_name='mailing',
            name='end_datetime',
            field=models.DateTimeField(blank=True, null=True, verbose_name='Дата и время окончание рассылки'),
        ),
    ]
