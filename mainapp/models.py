from django.db import models
import pytz
from django.core.validators import RegexValidator
from datetime import datetime
from django_celery_beat.models import PeriodicTask, IntervalSchedule
from .tasks import sendMailing


class Mailing(models.Model):
    start_datetime = models.DateTimeField(verbose_name="Дата и время запуска рассылки")
    message_text = models.TextField(verbose_name="Текст рассылки")
    client_filter_operator_code = models.CharField(max_length=10, verbose_name="Код оператора", blank=True, null=True)
    client_filter_tag = models.CharField(max_length=255, verbose_name="Тэг", blank=True, null=True)
    end_datetime = models.DateTimeField(verbose_name="Дата и время окончание рассылки", blank=True, null=True)

    def save(self, *args, **kwargs):
        is_new = not self.pk  # Check if the object is new (creating)
        super().save(*args, **kwargs)
        if is_new:
            print(self.start_datetime.tzinfo)
            if datetime.now().replace(tzinfo=self.start_datetime.tzinfo) > self.start_datetime:
                sendMailing.delay(self.pk)
            else:
                interval, _ = IntervalSchedule.objects.get_or_create(
                    every=100,
                    period=IntervalSchedule.DAYS,
                )
                PeriodicTask.objects.create(
                    interval=interval,
                    name=f'Рассылка #{self.pk}',
                    task='mainapp.tasks.sendMailing',
                    args=[self.pk],
                    start_time=self.start_datetime,
                    one_off=True,
                )


class PhoneNumberField(models.CharField):
    default_validators = [RegexValidator(
        regex=r'^7\d{10}$',
        message="Enter a valid phone number in the format: '7XXXXXXXXXX'"
    )]


class Client(models.Model):
    phone_number = PhoneNumberField(max_length=11, verbose_name='Номер телефона')
    operator_code = models.CharField(max_length=10, verbose_name='Код оператора')
    tag = models.CharField(max_length=255, verbose_name='Тэг', blank=True)
    timezone = models.CharField(max_length=255, choices=[(tz, tz) for tz in pytz.all_timezones], verbose_name='Часовой пояс')


class Message(models.Model):
    class Status(models.TextChoices):
        sent = True, 'Отправлено'
        notSent = False, 'Не отправлено'
        other = "Other", 'Другое'
    creation_datetime = models.DateTimeField(verbose_name="Дата и время создания", auto_now_add=True)
    delivery_status = models.CharField(max_length=255, verbose_name="Статус", choices=Status.choices, default=Status.other)
    mailing = models.ForeignKey(Mailing, on_delete=models.CASCADE)
    client = models.ForeignKey(Client, on_delete=models.CASCADE)
