from rest_framework import serializers
from mainapp.models import Client, Mailing, Message


class ClientSerializer(serializers.ModelSerializer):
    class Meta:
        model = Client
        fields = '__all__'


class MailingSerializer(serializers.ModelSerializer):
    class Meta:
        model = Mailing
        fields = '__all__'


class MailingStatisticsSerializer(serializers.Serializer):
    mailing_count = serializers.IntegerField()
    messages_count = serializers.IntegerField()
    sent_messages_count = serializers.IntegerField()
    unsent_messages_count = serializers.IntegerField()


class MailingDetailStatSerializer(serializers.ModelSerializer):
    messages_count = serializers.SerializerMethodField()
    sent_messages_count = serializers.SerializerMethodField()
    unsent_messages_count = serializers.SerializerMethodField()

    def get_messages_count(self, mailing):
        return mailing.message_set.count()

    def get_sent_messages_count(self, mailing):
        return mailing.message_set.filter(delivery_status=Message.Status.sent).count()

    def get_unsent_messages_count(self, mailing):
        return mailing.message_set.filter(delivery_status=Message.Status.notSent).count()


    class Meta:
        model = Mailing
        fields = ['id', 'start_datetime', 'message_text', 'client_filter_operator_code',
                  'client_filter_tag', 'end_datetime',  'messages_count', 'sent_messages_count',
                  'unsent_messages_count']
