from rest_framework import viewsets
from mainapp.models import Client, Mailing, Message
from .serilizers import ClientSerializer, MailingSerializer, MailingDetailStatSerializer, MailingStatisticsSerializer
from rest_framework.views import APIView
from rest_framework.response import Response
from django.db.models import Count


class ClientViewSet(viewsets.ModelViewSet):
    queryset = Client.objects.all()
    serializer_class = ClientSerializer


class MailingViewSet(viewsets.ModelViewSet):
    queryset = Mailing.objects.all()
    serializer_class = MailingSerializer


class MailingStatisticsView(APIView):
    def get(self, request):
        mailing_count = Mailing.objects.count()
        sent_messages_count = Message.objects.filter(delivery_status=Message.Status.sent).count()
        unsent_messages_count = Message.objects.filter(delivery_status=Message.Status.notSent).count()
        data = {
            'mailing_count': mailing_count,
            'messages_count': sent_messages_count+unsent_messages_count,
            'sent_messages_count': sent_messages_count,
            'unsent_messages_count': unsent_messages_count
        }

        serializer = MailingStatisticsSerializer(data)
        return Response(serializer.data)


class MailingDetailStatViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Mailing.objects.all()
    serializer_class = MailingDetailStatSerializer
