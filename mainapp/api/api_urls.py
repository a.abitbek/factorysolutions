from django.urls import path, include
from rest_framework import routers, permissions
from .api_views import ClientViewSet, MailingViewSet, MailingDetailStatViewSet, MailingStatisticsView
from drf_yasg.views import get_schema_view
from drf_yasg import openapi

router = routers.DefaultRouter()
router.register(r'clients', ClientViewSet)
router.register(r'mailing', MailingViewSet)
router.register(r'mailing-stats/detail', MailingDetailStatViewSet)

schema_view = get_schema_view(
    openapi.Info(
        title="API Тестовое задание для кандидатов-разработчиков ФР",
        default_version='v1',
        description="API Сервис уведомлений",
        # terms_of_service="https://www.example.com/terms/",
        contact=openapi.Contact(email="contact@example.com"),
        license=openapi.License(name="by aabitbekov"),
    ),
    public=True,
    permission_classes=[permissions.AllowAny],
)

urlpatterns = [
    path('api/docs/', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
    path('api/redoc/', schema_view.with_ui('redoc', cache_timeout=0), name='schema-redoc'),
    path('api/', include(router.urls)),
    path('api/mailing-statistics/', MailingStatisticsView.as_view(), name='mailing-statistics'),
]
